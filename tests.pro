TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /usr/local/include/
INCLUDEPATH += /usr/local/Cellar/boost/1.60.0_2/include/
INCLUDEPATH += /usr/local/Cellar/hiredis/0.13.3/include/
INCLUDEPATH += /usr/local/Cellar/libevent/2.0.22/include/

INCLUDEPATH += ../
INCLUDEPATH += ../include

LIBS += -L/usr/lib/x86_64-linux-gnu/
LIBS += -L/usr/local/lib/
LIBS += -L../lib/
LIBS += -L/usr/local/Cellar/boost/1.60.0_2/lib/
LIBS += -L/usr/local/Cellar/hiredis/0.13.3/lib/
LIBS += -L/usr/local/Cellar/libevent/2.0.22/lib/
LIBS += -L/usr/local/Cellar/zeromq/4.1.4/lib/
LIBS += -L/usr/local/Cellar/libsodium/1.0.12/lib/
LIBS += -lboost_regex -lboost_filesystem -lboost_stacktrace_backtrace -lboost_system -ldl  -lutil -lm
LIBS += -lzmq -levent -lhiredis -lsodium -lpthread -ldinemic

HEADERS += \
    scenarios/store_agent/testagentsimple.h \
    scenarios/store_agent/testparrot.h \
    scenarios/store_agent/testparrottrainer.h \
    scenarios/store_static/testcreate.h \
    scenarios/store_static/testlistenersimple.h \
    scenarios/store_static/testobjectperformance.h \
    scenarios/sync_dummy/testdummysyncrestore.h \
    scenarios/utils/testparseheaders.h \
    testbase.h \
    scenarios/store_static/testlists.h \
    scenarios/store_static/testlistenermulti.h \
    scenarios/store_static/testchildobject.h \
    check.h \
    scenarios/store_static/testlistnonexistent.h \
    scenarios/store_agent/testspy.h \
    scenarios/utils/testcryptstore.h \
    scenarios/utils/testcryptencrypt.h \
    scenarios/utils/testcryptkeycreate.h \
    scenarios/utils/testcryptsignature.h \
    scenarios/utils/testcrypthashsha512.h \
    scenarios/utils/testcryptlistmodels.h \
    scenarios/store_static/testupdateunauthorized.h \
    scenarios/store_static/testlistenersmanage.h \
    scenarios/utils/testsignatureheaders.h \
    scenarios/utils/testcryptduplicatekey.h \
    scenarios/store_static/testobjectmemoryconsumption.h \
    scenarios/utils/testheaderssignandverify.h \
    scenarios/store_static/teststoreobjectadd.h \
    scenarios/utils/testdappoptions.h \
    scenarios/utils/testdappcommands.h \
    scenarios/utils/testdappcommandmissingoption.h \
    scenarios/store_static/testfieldlistaccess.h \
    scenarios/store_static/testfieldaccess.h \
    scenarios/store_static/testfieldencrypt.h \
    scenarios/store_static/testfieldencryptanother.h \
    scenarios/store_agent/testparrotperformancetrainer.h \
    scenarios/utils/testvis.h \
    scenarios/store_static/testtwochains.h \
    scenarios/store_static/testobjectremove.h \
    testenv.h \
    scenarios/netdinemic/testusercreate.h \
    scenarios/utils/testcryptremovekey.h \
    scenarios/netdinemic/testnodecreate.h \
    scenarios/store_static/testfieldcaller.h \
    scenarios/store_static/testdlists.h \
    scenarios/rekonf/testrekonfnodecreate.h \
    scenarios/rekonf/testrekonfprepostactions.h \
    scenarios/rekonf/testrekonfseparateadminandfile.h \
    scenarios/store_static/testparentaccesschild.h \
    scenarios/utils/testwildcardtoregexp.h \
    scenarios/store_static/testdicts.h \
    scenarios/store_static/testddicts.h \
    scenarios/extensions/testdextensions.h \
    scenarios/utils/testdappcommandmissingoption.h

SOURCES += main.cpp \
    scenarios/store_agent/testagentsimple.cpp \
    scenarios/store_agent/testparrot.cpp \
    scenarios/store_agent/testparrottrainer.cpp \
    scenarios/store_static/testcreate.cpp \
    scenarios/store_static/testlistenersimple.cpp \
    scenarios/store_static/testobjectperformance.cpp \
    scenarios/sync_dummy/testdummysyncrestore.cpp \
    scenarios/utils/testparseheaders.cpp \
    testbase.cpp \
    scenarios/store_static/testlists.cpp \
    scenarios/store_static/testlistenermulti.cpp \
    scenarios/store_static/testchildobject.cpp \
    check.cpp \
    scenarios/store_static/testlistnonexistent.cpp \
    scenarios/store_agent/testspy.cpp \
    scenarios/utils/testcryptstore.cpp \
    scenarios/utils/testcryptencrypt.cpp \
    scenarios/utils/testcryptkeycreate.cpp \
    scenarios/utils/testcryptsignature.cpp \
    scenarios/utils/testcrypthashsha512.cpp \
    scenarios/utils/testcryptlistmodels.cpp \
    scenarios/store_static/testupdateunauthorized.cpp \
    scenarios/store_static/testlistenersmanage.cpp \
    scenarios/utils/testsignatureheaders.cpp \
    scenarios/utils/testcryptduplicatekey.cpp \
    scenarios/store_static/testobjectmemoryconsumption.cpp \
    scenarios/utils/testheaderssignandverify.cpp \
    scenarios/store_static/teststoreobjectadd.cpp \
    scenarios/utils/testdappoptions.cpp \
    scenarios/utils/testdappcommands.cpp \
    scenarios/utils/testdappcommandmissingoption.cpp \
    scenarios/store_static/testfieldlistaccess.cpp \
    scenarios/store_static/testfieldaccess.cpp \
    scenarios/store_static/testfieldencrypt.cpp \
    scenarios/store_static/testfieldencryptanother.cpp \
    scenarios/store_agent/testparrotperformancetrainer.cpp \
    scenarios/utils/testvis.cpp \
    scenarios/store_static/testtwochains.cpp \
    scenarios/store_static/testobjectremove.cpp \
    testenv.cpp \
    scenarios/netdinemic/testusercreate.cpp \
    scenarios/utils/testcryptremovekey.cpp \
    scenarios/netdinemic/testnodecreate.cpp \
    scenarios/store_static/testfieldcaller.cpp \
    scenarios/store_static/testdlists.cpp \
    scenarios/rekonf/testrekonfnodecreate.cpp \
    scenarios/rekonf/testrekonfprepostactions.cpp \
    scenarios/rekonf/testrekonfseparateadminandfile.cpp \
    scenarios/store_static/testparentaccesschild.cpp \
    scenarios/utils/testwildcardtoregexp.cpp \
    scenarios/store_static/testdicts.cpp \
    scenarios/store_static/testddicts.cpp \
    scenarios/extensions/testdextensions.cpp \
    scenarios/utils/testdappcommandmissingoption.cpp
