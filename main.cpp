/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <chrono>
#include <iostream>
#include <exception>
#include <unordered_map>
#include <vector>
#include <time.h>
#include <boost/stacktrace.hpp>

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include <libdinemic/store/storeinterface.h>
#include <libdinemic/store/redisdriver.h>
#include <libdinemic/store/memorydriver.h>
//#include <libdinemic/sync/chargersync.h>
#include <libdinemic/sync/dummysync.h>
#include <libdinemic/sync/zeromqsync.h>

#include <tests/scenarios/utils/testdappoptions.h>
#include <tests/scenarios/utils/testdappcommands.h>
#include <tests/scenarios/utils/testdappcommandmissingoption.h>
#include <tests/scenarios/utils/testcrypthashsha512.h>
#include <tests/scenarios/utils/testparseheaders.h>
#include <tests/scenarios/utils/testcryptstore.h>
#include <tests/scenarios/utils/testcryptremovekey.h>
#include <tests/scenarios/utils/testcryptkeycreate.h>
#include <tests/scenarios/utils/testcryptencrypt.h>
#include <tests/scenarios/utils/testcryptsignature.h>
#include <tests/scenarios/utils/testcryptlistmodels.h>
#include <tests/scenarios/utils/testsignatureheaders.h>
#include <tests/scenarios/utils/testcryptduplicatekey.h>
#include <tests/scenarios/utils/testheaderssignandverify.h>
#include <tests/scenarios/utils/testwildcardtoregexp.h>
#include <tests/scenarios/sync_dummy/testdummysyncrestore.h>
#include <tests/scenarios/store_static/testlistenersmanage.h>
#include <tests/scenarios/store_static/testlistnonexistent.h>
#include <tests/scenarios/store_static/testlists.h>
#include <tests/scenarios/store_static/testdicts.h>
#include <tests/scenarios/store_static/testfieldaccess.h>
#include <tests/scenarios/store_static/testfieldcaller.h>
#include <tests/scenarios/store_static/testfieldlistaccess.h>
#include <tests/scenarios/store_static/testfieldencrypt.h>
#include <tests/scenarios/store_static/testfieldencryptanother.h>
#include <tests/scenarios/store_static/testdlists.h>
#include <tests/scenarios/store_static/testddicts.h>
#include <tests/scenarios/store_static/testcreate.h>
#include <tests/scenarios/store_static/testobjectremove.h>
#include <tests/scenarios/store_static/testchildobject.h>
#include <tests/scenarios/store_static/testtwochains.h>
#include <tests/scenarios/store_static/testlistenersimple.h>
#include <tests/scenarios/store_static/testlistenermulti.h>
#include <tests/scenarios/store_static/testobjectperformance.h>
#include <tests/scenarios/store_static/testobjectmemoryconsumption.h>
#include <tests/scenarios/store_static/testupdateunauthorized.h>
#include <tests/scenarios/store_static/teststoreobjectadd.h>
#include <tests/scenarios/store_static/testparentaccesschild.h>
#include <tests/scenarios/store_agent/testagentsimple.h>
#include <tests/scenarios/store_agent/testparrottrainer.h>
#include <tests/scenarios/store_agent/testparrot.h>
#include <tests/scenarios/store_agent/testparrotperformancetrainer.h>
#include <tests/scenarios/store_agent/testspy.h>
#include <tests/scenarios/extensions/testdextensions.h>

#include <tests/scenarios/netdinemic/testusercreate.h>
#include <tests/scenarios/netdinemic/testnodecreate.h>

#include <tests/scenarios/rekonf/testrekonfnodecreate.h>
#include <tests/scenarios/rekonf/testrekonfprepostactions.h>
#include <tests/scenarios/rekonf/testrekonfseparateadminandfile.h>

#include <tests/scenarios/utils/testvis.h>


using namespace std;
using namespace std::chrono;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

#include <sodium.h>

namespace Dinemic {
    namespace Store {
        int test(int argc, char **argv)
        {
            try {
                char *local_config = getenv("DINEMIC_CONFIG");

                if (local_config != NULL) {
                    cerr << "Opening config " << local_config << endl;
                    config.open(local_config);
                }

                string loglevel = config.get("LOGLEVEL", "error");
                cerr << "Loglevel for tests is: " << loglevel << endl;
                if (loglevel == "verbose")
                    set_loglevel(loglevel_verbose);
                else if (loglevel == "action")
                    set_loglevel(loglevel_action);
                else if (loglevel == "debug")
                    set_loglevel(loglevel_debug);
                else if (loglevel == "info")
                    set_loglevel(loglevel_info);
                else if (loglevel == "error")
                    set_loglevel(loglevel_error);
                else if (loglevel == "none")
                    set_loglevel(loglevel_none);
                else {
                    cerr << "Unknown loglevel " << loglevel << endl;
                    set_loglevel(loglevel_error);
                }

                srand(time(0));
                DEBUG("Initializing drivers");
                StoreInterface *store = NULL;
                string configured_store = config.get("STORE_DRIVER", "MemoryDriver");
                if (configured_store == "RedisDriver") {
                    INFO("Using RedisDriver as store with local server");
                    store = new RedisDriver();
                } else {
                    INFO("Using default MemoryDriver as store");
                    store = new MemoryDriver();
                }

                ZeroMQSync sync_zmq(store);
                DummySync sync_dummy(store);

                if (argc >= 2) {
                    if (string(argv[1]) == "trainer-zmq") {
                        sync_zmq.start_agent();
                        sync_zmq.wait_running();

                        TestParrotTrainer tpt(&sync_zmq, store);
                        TestParrotPerformanceTrainer tppt(&sync_zmq, store);

                        tpt.prepare();
                        tpt.execute();
                        tpt.cleanup();

                        tppt.prepare();
                        tppt.execute();
                        tppt.cleanup();

                        sync_zmq.stop_agent();
                    } else if (string(argv[1]) == "parrot-zmq") {
                        sync_zmq.start_agent();
                        sync_zmq.wait_running();

                        TestParrot tp(&sync_zmq, store);

                        tp.prepare();
                        tp.execute();
                        tp.cleanup();

                        sync_zmq.stop_agent();
                    } else if (string(argv[1]) == "spy-zmq") {
                        sync_zmq.start_agent();
                        sync_zmq.wait_running();

                        TestSpy ts(&sync_zmq, store);

                        ts.prepare();
                        ts.execute();
                        ts.cleanup();

                        sync_zmq.stop_agent();
                    } else if (string(argv[1]) == "vis") {
                        TestVis tv(&sync_dummy, store);
                        tv.execute();
//                    } else if (string(argv[1]) == "get-update") {
//                        cerr << store->update_get(argv[2]).to_string() << endl;
                    } else if (string(argv[1]) == "cleanup") {
                        cerr << "Removing all data" << endl;
                        Utils::exec(string("rm -rf ") + config.get("KEYRING_DIR", "/not_set"));
                        vector<string> keys = store->keys(NULL, "*");
                        for (auto key : keys) {
                            store->del(NULL, key);
                        }
                    } else if (string(argv[1]) == "test") {
                        vector<TestBase*> tests;
                        if (string(argv[2]) == "dapp-options") {
                            tests.push_back(new TestDAppOptions(&sync_dummy, store));
                            tests.push_back(new TestDAppCommands(&sync_dummy, store));
                            tests.push_back(new TestDAppCommandMissingOption(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "crypt") {
                            tests.push_back(new TestCryptHashSHA512(&sync_dummy, store));
                            tests.push_back(new TestCryptStore(&sync_dummy, store));
                            tests.push_back(new TestCryptRemoveKey(&sync_dummy, store));
                            tests.push_back(new TestCryptKeyCreate(&sync_dummy, store));
                            tests.push_back(new TestCryptEncrypt(&sync_dummy, store));
                            tests.push_back(new TestCryptSignature(&sync_dummy, store));
                            tests.push_back(new TestCryptListModels(&sync_dummy, store));
                            tests.push_back(new TestCryptDuplicateKey(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "headers") {
                            tests.push_back(new TestParseHeaders(&sync_dummy, store));
                            tests.push_back(new TestHeadersSignAndVerify(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "lists") {
                            tests.push_back(new TestDLists(&sync_dummy, store));
                            tests.push_back(new TestLists(&sync_dummy, store));
                            tests.push_back(new TestListNonExistent(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "dicts") {
                            tests.push_back(new TestDicts(&sync_dummy, store));
                            tests.push_back(new TestDDicts(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "fields") {
                            tests.push_back(new TestFieldAccess(&sync_dummy, store));
                            tests.push_back(new TestFieldCaller(&sync_dummy, store));
                            tests.push_back(new TestFieldListAccess(&sync_dummy, store));
                            tests.push_back(new TestFieldEncrypt(&sync_dummy, store));
                            tests.push_back(new TestFieldEncryptAnother(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "dmodel") {
                            tests.push_back(new TestCreate(&sync_dummy, store));
                            tests.push_back(new TestObjectRemove(&sync_dummy, store));
                            tests.push_back(new TestChildObject(&sync_dummy, store));
                            tests.push_back(new TestParentAccessChild(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "sync") {
                            tests.push_back(new TestDummySyncRestore(&sync_dummy, store));
                            tests.push_back(new TestTwoChains(&sync_dummy, store));
                            tests.push_back(new TestSignatureHeaders(&sync_dummy, store));
                            tests.push_back(new TestUpdateUnauthorized(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "listeners") {
                            tests.push_back(new TestWildcardToRegexp(&sync_dummy, store));
                            tests.push_back(new TestListenerSimple(&sync_dummy, store));
                            tests.push_back(new TestListenerMulti(&sync_dummy, store));
                            tests.push_back(new TestListenersManage(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "preformance") {
                            tests.push_back(new TestStoreObjectAdd(&sync_dummy, store));
                            tests.push_back(new TestObjectMemoryConsumption(&sync_dummy, store));
                            tests.push_back(new TestObjectPerformance(&sync_dummy, store));
                        }
                        if (string(argv[2]) == "dextensions") {
                            tests.push_back(new TestDExtensions(&sync_dummy, store));
                        }

                        if (string(argv[2]) == "netdinemic") {
                            tests.push_back(new TestNetDinemicUserCreate(&sync_dummy, store));
                            tests.push_back(new TestNetDinemicNodeCreate(&sync_dummy, store));
                        }

                        if (string(argv[2]) == "rekonf") {
                            tests.push_back(new TestRekonfNodeCreate(&sync_dummy, store));
                            tests.push_back(new TestRekonfPrePostActions(&sync_dummy, store));
                            tests.push_back(new TestRekonfSeparateAdminAndFile(&sync_dummy, store));
                        }

                        for (auto test : tests) {
                            cerr << "Starting test: " << test->get_name() << " (" << test->get_description() << ")" << endl;
                            test->prepare();
                            try {
                                high_resolution_clock::time_point t1 = high_resolution_clock::now();
                                test->execute();
                                high_resolution_clock::time_point t2 = high_resolution_clock::now();
                                auto duration = duration_cast<microseconds>( t2 - t1 ).count();
                                cerr << "\tDONE in " << ((double)duration/1000000) << "s" << endl;
                            } catch (DException &e) {
                                cerr << "\tFAILED! " << test->get_name() << ": " << e.get_reason() << endl;

                                for (auto list : store->dump_lists()) {
                                    cerr << "\tList " << list.first << endl;
                                    for (string item : list.second) {
                                        cerr << "\t - " << item << endl;
                                    }
                                }

                                for (auto key : store->dump_keys()) {
                                    cerr << "\tStore: " << key << endl;
                                }
                                return 1;
                            } catch (exception &e) {
                                cerr << "\tFAILED! " << test->get_name() << ": " << e.what() << endl;

                                cerr << boost::stacktrace::stacktrace() << endl;

                                for (auto list : store->dump_lists()) {
                                    cerr << "\tList " << list.first << endl;
                                    for (string item : list.second) {
                                        cerr << "\t - " << item << endl;
                                    }
                                }

                                for (auto key : store->dump_keys()) {
                                    cerr << "\tStore: " << key << endl;
                                }
                                return 1;
                            }

                            try {
                                test->cleanup();
                            } catch (exception &e) {
                                cerr << "\t CLEANUP FAILED! " << test->get_name() << ": " << e.what() << endl;
                                return 1;
                            }

                        }
                    }
                }

                sleep(1);
                return 0;
            } catch (DException &e) {
                cerr << "Error: " << e.get_reason() << endl;
                return 1;
            } catch (exception &e) {
                cerr << "Catched exception: " << endl;
                cerr << "\t" << e.what() << endl;
                return 1;
            }
            sleep(1);
        }
    }
}

int main(int argc, char **argv) {
    return test(argc, argv);
}
