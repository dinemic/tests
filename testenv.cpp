/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testenv.h"

using namespace std;
using namespace Dinemic;

TestEnv::TestEnv()
{

}

TestEnv::~TestEnv() {
    kill_procs();
}

void TestEnv::kill_procs() {
    for (pid_t pid : children) {
        int s;
        cerr << "Killing " << pid << endl;
        kill(pid, 9);
        waitpid(pid, &s, NULL);
        Utils::exec("killall " + to_string(pid));
    }
}

void TestEnv::add_var(const string &name, const string &value) {
    variables[name] = value;
}

string TestEnv::call_sync(const string &cmd) {
    for (auto v : variables)
        setenv(v.first.c_str(), v.second.c_str(), true);
    return Utils::exec(cmd);
}

void TestEnv::call_async(const string &cmd) {
    pid_t child = fork();
    if (child == 0) {
        for (auto v : variables)
            setenv(v.first.c_str(), v.second.c_str(), true);
        Utils::exec(cmd);
        exit(0);
    } else {
        children.push_back(child);
    }
}
