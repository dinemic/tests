#include "testdextensions.h"

using namespace std;

TestExtension::TestExtension(Dinemic::DApp *app)
    : Dinemic::DExtension(app),
      on_call_triggered(false),
      on_set_triggered(false),
      on_get_triggered(false),
      get_info_triggered(false),
      get_license_triggered(false)
{

}

void TestExtension::on_call(string function, string value) {
    if (function == "test") {
        on_call_triggered = true;
    }
}

string TestExtension::on_get(string key) {
    if (key == "test") {
        on_get_triggered = true;
    }
    return Dinemic::DExtension::on_get(key);
}

void TestExtension::on_set(string key, string value) {
    if (key == "test") {
        on_set_triggered = true;
    }

    Dinemic::DExtension::on_set(key, value);
}

string TestExtension::get_info() {
    get_info_triggered = true;
    return "test";
}

string TestExtension::get_license() {
    get_license_triggered = true;
    return "test";
}

TestDExtensions::TestDExtensions(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestDExtensions", "Test extensions workflow, adding them and calls")
{

}

void TestDExtensions::execute()
{
    Dinemic::DApp app(0, NULL, "", "");
    TestExtension ext1(&app), ext2(&app);
    Dinemic::DExtension::addExtension(&ext1, "one");
    Dinemic::DExtension::addExtension(&ext2, "two");

    Dinemic::DExtension::call("one", "test", "foo");

    Check(ext1.on_call_triggered == true);
    Check(ext2.on_call_triggered == false);

    Dinemic::DExtension::call("*", "test", "foo");

    Check(ext1.on_call_triggered == true);
    Check(ext2.on_call_triggered == true);

    Check(ext1.on_get_triggered == false);
    Check(ext1.on_get_triggered == false);

    Dinemic::DExtension::set("one", "test", "foo");

    Check(ext1.on_set_triggered == true);
    Check(ext2.on_set_triggered == false);

    Dinemic::DExtension::set("*", "test", "foo");

    Check(ext1.on_set_triggered == true);
    Check(ext2.on_set_triggered == true);
}
