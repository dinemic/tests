#ifndef TESTDEXTENSIONS_H
#define TESTDEXTENSIONS_H

#include <testbase.h>
#include <check.h>

#include <libdinemic/dextension.h>
#include <libdinemic/dapp.h>


class TestExtension : public Dinemic::DExtension {
public:
    TestExtension(Dinemic::DApp *app);
    void on_call(std::string function, std::string value);
    void on_set(std::string key, std::string value);
    std::string on_get(std::string key);
    std::string get_info();
    std::string get_license();

    bool on_call_triggered;
    bool on_set_triggered;
    bool on_get_triggered;
    bool get_info_triggered;
    bool get_license_triggered;
};

class TestDExtensions : public TestBase
{
public:
    TestDExtensions(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store);
    void execute();
};

#endif // TESTDEXTENSIONS_H
