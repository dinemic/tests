/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testparrotperformancetrainer.h"

using namespace std;

string parrot_performance_value_new, parrot_performance_value_updated;

TestParrotPerformanceTrainer::TestParrotPerformanceTrainer(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestParrotPerformanceTrainer", "Generate 1000 updates and wait for confirmation from other node")
{

}

void TestParrotPerformanceTrainer::prepare() {
    ppt = new ParrotPerformanceTrainer();
}

bool check(std::string a, int max_value) {
    a = a + ".";
    for (int i = 0; i < max_value; i++) {
        string f = string(".") + to_string(i) + string(".");
        if (a.find(f) == string::npos) {
            INFO("Don't have " + f)
            return false;
        }
    }
    return true;
}

void TestParrotPerformanceTrainer::execute() {
    Dinemic::DModel model("ParrotPerformanceTrainer", store, sync, vector<string>());
    sync->add_on_updated_listener("ParrotPerformanceTrainer:*", ppt);

    for (int i = 0; i < PERFORMANCE_ITERATIONS; i++) {
        model.set("NEW_VALUE", to_string(i));
    }

    while (parrot_performance_value_new.size() < PERFORMANCE_ITERATIONS) {
        sleep(1);
    }
    INFO("Long enough... waiting for compare");

    while (!check(parrot_performance_value_new, PERFORMANCE_ITERATIONS) && !check(parrot_performance_value_updated, PERFORMANCE_ITERATIONS)) {
        sleep(1);
        INFO("Waiting for values...")
    }
    INFO("Parrot Performance Trainer Done");
}

void TestParrotPerformanceTrainer::cleanup() {
    sync->remove_on_updated_listener("ParrotPerformanceTrainer:*", ppt);
    delete ppt;
}
