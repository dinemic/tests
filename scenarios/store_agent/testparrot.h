/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TESTPARROT_H
#define TESTPARROT_H

#include <tests/testbase.h>
#include <libdinemic/logger.h>
#include <libdinemic/dmodel.h>
#include <libdinemic/daction.h>

class Parrot: public Dinemic::DAction
{
    void on_update(Dinemic::DActionContext &flags, const std::string &key, const std::string &old_value, const std::string &new_value) {
        if (key == "value_Phrase") {
            Dinemic::DModel parrot_reply(flags.get_headers()["Caller"], flags.get_object().get_store_interface(), flags.get_object().get_sync_interface());
            parrot_reply.set("UPDATE_DONE", flags.get_headers()["Caller"]);
        }
    }
};

class ParrotPerformance: public Dinemic::DAction
{
    void on_update(Dinemic::DActionContext &flags, const std::string &key, const std::string &old_value, const std::string &new_value) {
        if (flags.is_verified()) {
            if (key == "value_NEW_VALUE") {
                flags.get_object().set("UPDATE_VALUE", new_value);
            } else
                ERROR("Unknown key " + key);
        } else {
            ERROR("Update not verified");
        }
    }
};


class TestParrot : public TestBase
{
    Parrot *p;
    ParrotPerformance *pp;
public:
    TestParrot(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store);
    void prepare();
    void execute();
    void cleanup();
};

#endif // TESTPARROT_H
