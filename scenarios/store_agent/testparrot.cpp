/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testparrot.h"

TestParrot::TestParrot(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestParrot", "Create new object in database on each update from ParrotTrainer")
{

}

void TestParrot::prepare() {
    INFO("Creating listener object");
    p = new Parrot();
    pp = new ParrotPerformance();
}

void TestParrot::execute() {
    INFO("Adding listener to sync");
    sync->add_on_update_listener("ParrotTrainer:*", p);
    sync->add_on_update_listener("ParrotPerformanceTrainer:*:value_NEW_VALUE", pp);

    INFO("Waiting");
    while (1) {
        sleep(1);
    }
}

void TestParrot::cleanup() {
    sync->remove_on_update_listener("ParrotTrainer:*", p);
    sync->remove_on_update_listener("ParrotPerformanceTrainer:*:value_NEW_VALUE", pp);
    delete p;
    delete pp;
}
