/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TESTDUMMYSYNCRESTORE_H
#define TESTDUMMYSYNCRESTORE_H

#include <testbase.h>
#include <libdinemic/sync/dummysync.h>
#include <libdinemic/store/memorydriver.h>

class TestDummySyncRestore : public TestBase
{
    Dinemic::Store::MemoryDriver *store_primary;
    Dinemic::Store::MemoryDriver *store_secondary;
    Dinemic::Sync::DummySync *sync_primary;
    Dinemic::Sync::DummySync *sync_secondary;

    std::string object_id;

public:
    TestDummySyncRestore(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store);
    void prepare();
    void execute();
    void cleanup();
};

#endif // TESTDUMMYSYNCRESTORE_H
