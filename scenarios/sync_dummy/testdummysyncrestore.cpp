/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testdummysyncrestore.h"

using namespace std;

TestDummySyncRestore::TestDummySyncRestore(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestDummySyncRestore", "Test DummySync driver to check if restoring updates works properly")
{

}

void TestDummySyncRestore::prepare() {
    store_primary = new Dinemic::Store::MemoryDriver();
    sync_primary = new Dinemic::Sync::DummySync(store_primary);

    Dinemic::DModel object("Object", store_primary, sync_primary, vector<string>());
    object_id = object.get_db_id();

    object.set("SomeKey", "SomeValue");

    object.list_append("SimpleList", "1");
    object.list_append("SimpleList", "2");
    object.list_append("SimpleList", "3");

    object_id = object.get_db_id();

    while(sync->process_queue());

    delete sync_primary;
    delete store_primary;

    sync_primary = NULL;
    store_primary = NULL;
}

void TestDummySyncRestore::execute() {
    store_secondary = new Dinemic::Store::MemoryDriver();
    sync_secondary = new Dinemic::Sync::DummySync(store_secondary);

    while (sync_secondary->process_queue());

    Dinemic::DModel object(object_id, store_secondary, sync_secondary);
    assert(object.get("SomeKey", "unknown") == "SomeValue");
    assert(object.list_at("SimpleList", 0) == "1");
    assert(object.list_at("SimpleList", 1) == "2");
    assert(object.list_at("SimpleList", 2) == "3");
}

void TestDummySyncRestore::cleanup() {
    if (sync_primary != NULL)
        delete sync_primary;
    if (store_primary != NULL)
        delete store_primary;
    delete sync_secondary;
    delete store_secondary;
}
