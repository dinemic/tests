/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testtwochains.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

Dinemic::Store::TestTwoChains::TestTwoChains(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestTwoChains", "Check if creting two object causes creating two independent chains")
{

}

void Dinemic::Store::TestTwoChains::execute()
{
    DModel node1("Node1", store, sync, std::vector<std::string>());
    DModel node2("Node2", store, sync, std::vector<std::string>());

    while (sync->process_queue());

    MemoryDriver *md = (MemoryDriver*) store;

    if (config.get("CHAINS", CHAINS_MULTIPLE) == CHAINS_MULTIPLE) {
        assert(md->last_update[node1.get_db_id()][HEADER_OBJECT] == node1.get_db_id());
        assert(md->last_update[node2.get_db_id()][HEADER_OBJECT] == node2.get_db_id());
    } else {
        assert(md->last_update["single_chain"][HEADER_OBJECT] == node2.get_db_id());
    }
}
