/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testfieldcaller.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestFieldCallerObject::TestFieldCallerObject(const std::string &db_id, StoreInterface *store, SyncInterface *sync, DModel *parent, DModel *caller)
    : DModel(db_id, store, sync, parent, caller),
      prop_encrypted(this, "prop_encrypted", true),
      prop_authorized(this, "prop_authorized", false)
{

}

TestFieldCallerObject::TestFieldCallerObject(DModel *parent)
    : DModel("TestFieldCallerObject", parent),
      prop_encrypted(this, "prop_encrypted", true),
      prop_authorized(this, "prop_authorized", false)
{

}

TestFieldCallerObject::TestFieldCallerObject(StoreInterface *store, SyncInterface *sync, const std::vector<std::string> &authorized_keys)
    : DModel("TestFieldCallerObject", store, sync, authorized_keys),
      prop_encrypted(this, "prop_encrypted", true),
      prop_authorized(this, "prop_authorized", false)
{

}


TestFieldCaller::TestFieldCaller(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestFieldCaller", "Test cross object access to fields. In this test one object accesses field of another, encrypted with update_authorized_keys set")
{

}

void TestFieldCaller::prepare() {

}
void TestFieldCaller::execute() {
    TestFieldCallerObject to_encrypted(store, sync, vector<string>());
    TestFieldCallerObject to_authorized(store, sync, vector<string>());
    TestFieldCallerObject to(store, sync, vector<string>());

    to.read_authorized_objects.append(to_encrypted.get_db_id());
    to.update_authorized_objects.append(to_authorized.get_db_id());

    DAppHelpers::IgnoreUnsignedUpdates firewall;
    firewall.apply(sync, to.get_db_id());

    while (sync->process_queue());
    sleep(1);

    to.prop_authorized = "x";
    to.prop_encrypted = "y";

    while (sync->process_queue());
    sleep(1);

    // Check if read_authorized can decrypt
    TestFieldCallerObject to_recreated_read(to.get_db_id(), store, sync, NULL, &to_encrypted);
    Check(to_recreated_read.prop_encrypted.to_string() == "y");

    // Check if update_authorized can update field
    TestFieldCallerObject to_recreated_update(to.get_db_id(), store, sync, NULL, &to_authorized);
    to_recreated_update.prop_authorized = "z";

    while (sync->process_queue());
    Check(to.prop_authorized.to_string() == "z");
}
void TestFieldCaller::cleanup() {

}
