/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testlistnonexistent.h"

using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestListNonExistent::TestListNonExistent(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestListNonExistent", "Test behavior for non-existent lists, items and other items")
{

}

void TestListNonExistent::execute() {
    DModel node("Node", store, sync, std::vector<std::string>());

    assert(node.list_length("non-existent") == 0);

    try {
        node.list_at("non-existent", 0);
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }
    try {
        node.list_at("non-existent", 1);
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }
    try {
        node.list_at("non-existent", -1);
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }

    node.list_append("something", "abc");
    while (sync->process_queue());

    assert(node.list_at("something", 0) == "abc");

    try {
        node.list_at("something", 1);
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }
    try {
        node.list_at("something", -1);
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }

    node.list_set("something", 0, "def");
    while (sync->process_queue());

    assert(node.list_at("something", 0) == "def");
    try {
        node.list_at("something", 1);
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }
    try {
        node.list_at("something", -1);
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }
    try {
        node.list_set("whatever", 1, "def");
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }

    try {
        // save in non existent list
        node.list_set("another", 0, "abc");
        assert(0 == 1);
    } catch (DModelElementNotFound e) {

    }
}
