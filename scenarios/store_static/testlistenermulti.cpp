/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testlistenermulti.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestListenerMulti::TestListenerMulti(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "ListenerMulti", "Create multiple listeners for one update type")
{

}

void TestListenerMulti::prepare() {
    spy_a = new TestSpyA();
    spy_b = new TestSpyB();
    spy_c = new TestSpyC();
}

void TestListenerMulti::execute() {
    sync->add_on_create_listener("Node:*", spy_a);
    sync->add_on_create_listener("Node:*", spy_b);
    sync->add_on_create_listener("Node:*", spy_c);
    sync->add_on_created_listener("Node:*", spy_a);
    sync->add_on_created_listener("Node:*", spy_b);
    sync->add_on_created_listener("Node:*", spy_c);

    DModel node("Node", store, sync, vector<string>());
    DModel not_node("NotNode", store, sync, vector<string>());

    sync->add_on_update_listener(node.get_db_id() + ":value_state", spy_a);
    sync->add_on_update_listener(node.get_db_id() + ":value_state", spy_b);
    sync->add_on_update_listener(node.get_db_id() + ":value_state", spy_c);
    sync->add_on_updated_listener("NotNode:*:value_state", spy_a);
    sync->add_on_updated_listener("NotNode:*:value_state", spy_b);
    sync->add_on_updated_listener("NotNode:*:value_state", spy_c);

    not_node.set("state", "initializing too");

    // Here process queue
    while (sync->process_queue());

    // Here process notifications created by listener
    while (sync->process_queue());

    assert(node.get("CREATE_A", "none") == "true");
    assert(node.get("CREATE_B", "none") == "true");
    assert(node.get("CREATE_C", "none") == "true");
    assert(node.get("CREATED_A", "none") == "true");
    assert(node.get("CREATED_B", "none") == "true");
    assert(node.get("CREATED_C", "none") == "true");
    assert(node.get("UPDATE_A", "none") == "none");
    assert(node.get("UPDATE_B", "none") == "none");
    assert(node.get("UPDATE_C", "none") == "none");
    assert(not_node.get("UPDATED_A", "none") == "true");
    assert(not_node.get("UPDATED_B", "none") == "true");
    assert(not_node.get("UPDATED_C", "none") == "true");

    node.set("state", "initializing");

    while (sync->process_queue());
    while (sync->process_queue());

    assert(node.get("UPDATE_A", "none") == "true");
    assert(node.get("UPDATE_B", "none") == "true");
    assert(node.get("UPDATE_C", "none") == "true");
};
