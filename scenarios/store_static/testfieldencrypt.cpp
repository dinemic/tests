/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testfieldencrypt.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestFieldEncryptObject::TestFieldEncryptObject(const string &db_id, StoreInterface *store, SyncInterface *sync)
    : DModel(db_id, store, sync),
      prop1(this, "prop1", true)
{

}


TestFieldEncryptObject::TestFieldEncryptObject(DModel *parent)
    : DModel("TestFieldEncryptObject", parent),
    prop1(this, "prop1", true)
{

}


TestFieldEncryptObject::TestFieldEncryptObject(StoreInterface *store,
                       SyncInterface *sync,
                       const vector<string> &authorized_keys)
    : DModel("TestFieldEncryptObject", store, sync, authorized_keys),
      prop1(this, "prop1", true)
{
}


TestFieldEncrypt::TestFieldEncrypt(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestFieldEncrypt", "Test field encryption")
{

}


void TestFieldEncrypt::execute() {
    TestFieldEncryptObject to(store, sync, vector<string>());

    while (sync->process_queue());
    while (sync->process_queue());

    to.prop1 = "Ala ma kota";

    while (sync->process_queue());
    while (sync->process_queue());

    assert(to.get("prop1") != "Ala ma kota");
    assert(Utils::split(to.get("prop1"), "#").size() == 2);
    assert(to.get("prop1") != "Ala ma kota");

    string value = to.prop1.to_string();

    assert(value == "Ala ma kota");
}
