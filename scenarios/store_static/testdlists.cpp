/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testdlists.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestDListsObject::TestDListsObject(const string &db_id, StoreInterface *store, SyncInterface *sync)
    : DModel(db_id, store, sync),
      list_encrypted(this, "ListEncrypted", true),
      list_public(this, "ListPublic", false)
{
}

TestDListsObject::TestDListsObject(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_keys)
    : DModel("TestDListsObject", store, sync, authorized_keys),
      list_encrypted(this, "ListEncrypted", true),
      list_public(this, "ListPublic", false)
{

}

TestDLists::TestDLists(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestDLists", "Test encryption and basic functions of DLists")
{

}

void TestDLists::execute() {
    TestDListsObject to(store, sync, vector<string>());

    // Let's process object creation first, to get encryption keys
    while (sync->process_queue());
    while (sync->process_queue());

    to.list_encrypted.append("a").append("b").append("c");
    to.list_public.append("a").append("b").append("c");

    while (sync->process_queue());
    while (sync->process_queue());

    Check(to.list_public.get(0) == "a");
    Check(to.list_public.get(2) == "c");

    Check(to.list_encrypted.get(0) == "a");
    Check(to.list_encrypted.get(2) == "c");

    Check(to.list_public.index("b") == 1);
    Check(to.list_encrypted.index("b") == 1);
}
