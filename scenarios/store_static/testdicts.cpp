/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "testdicts.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestDicts::TestDicts(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestDicts", "Test dictionary functionality in DModel")
{

}

void TestDicts::execute()
{
    DModel test_object("TestDictsObject", store, sync, vector<string>());

    while (sync->process_queue());

    Check(test_object.dict_get("test_dict", "x", "abc") == "abc");

    test_object.dict_set("test_dict", "a", "1");
    test_object.dict_set("test_dict", "b", "2");
    test_object.dict_set("test_dict_2", "c", "1");

    while (sync->process_queue());

    Check(test_object.dict_get("test_dict", "a", "none") == "1");
    Check(test_object.dict_get("test_dict", "b", "none") == "2");
    Check(test_object.dict_get("test_dict", "c", "none") == "none");

    Check(test_object.dict_get("test_dict_2", "c", "none") == "1");

    Check(test_object.dict_keys("test_dict").size() == 2);
    Check(test_object.dict_keys("test_dict_2").size() == 1);
    Check(test_object.dict_keys("test_dict_3").size() == 0);

    test_object.dict_set("test_dict", "b", "B");

    while (sync->process_queue());

    Check(test_object.dict_get("test_dict", "b", "none") == "B");
}
