/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testupdateunauthorized.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestUpdateUnauthorized::TestUpdateUnauthorized(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "UpdateUnauthorized", "Check if update without valid key could will be properly processed. The verified flag should be false")
{

}

void TestUpdateUnauthorized::prepare() {
    Utils::exec("rm -rf " + config.get("KEYRING_DIR"));
}

void TestUpdateUnauthorized::execute() {
    DCrypt dc1, dc2;
    // Create one model
    DModel model("TestUpdateUnauthorizedModel", store, sync, vector<string>());

    // Update some field
    model.set("a", "1");

    while (sync->process_queue());
    while (sync->process_queue());

    assert(model.get("a") == "1");

    // Remove Keyring dir
    Utils::exec("rm -rf \"" + config.get("KEYRING_DIR") + "\"");

    // Create sync2 without cached keys
    sync2 = new DummySync(store);

    helper = new DAppHelpers::IgnoreUnsignedUpdates();
    helper->apply(sync2, "TestUpdateUnauthorizedModel:*");

    // Recreate this model again, with empty keyring and new sync, without cached keys
    dcrypt.clear_cache();
    Utils::exec("rm -rf " + config.get("KEYRING_DIR"));

    DModel modelB(model.get_db_id(), store, sync2);
    modelB.set("a", "2");

    while (sync2->process_queue());
    while (sync2->process_queue());

    assert(modelB.get("a") == "1");
}

void TestUpdateUnauthorized::cleanup() {
    // TODO: Remove helper from sync2
    helper->revoke(sync2, "TestUpdateUnauthorizedModel:*");
    delete helper;
}
