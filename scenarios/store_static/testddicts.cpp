#include "testddicts.h"

using namespace std;

using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestDDictsObject::TestDDictsObject(const string &db_id, StoreInterface *store, SyncInterface *sync)
    : DModel(db_id, store, sync),
      dict_encrypted(this, "DictEncrypted", true),
      dict_public(this, "DictPublic", false)
{
}

TestDDictsObject::TestDDictsObject(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_keys)
    : DModel("TestDDictsObject", store, sync, authorized_keys),
      dict_encrypted(this, "DictEncrypted", true),
      dict_public(this, "DictPublic", false)
{

}


TestDDicts::TestDDicts(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestDDicts", "Test DDict field for its base functionality")
{

}

void TestDDicts::execute() {
    TestDDictsObject to(store, sync, vector<string>());

    // Let's process object creation first, to get encryption keys
    while (sync->process_queue());
    while (sync->process_queue());

    to.dict_encrypted.set("a", "x");
    to.dict_encrypted.set("b", 1);
    to.dict_encrypted.set("c", 2.2);

    while (sync->process_queue());
    while (sync->process_queue());

    Check(to.dict_encrypted.get("a", "") == "x");
    Check(to.dict_encrypted.get("b", "") == "1");
    Check(to.dict_encrypted.get("c", "") == "2.200000");
    Check(to.serialize().find("DictEncrypted") != string::npos);
}
