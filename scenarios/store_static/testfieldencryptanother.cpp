/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testfieldencryptanother.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestFieldEncryptAnotherObject::TestFieldEncryptAnotherObject(const string &db_id, StoreInterface *store, SyncInterface *sync)
    : DModel(db_id, store, sync),
      prop1(this, "prop1", true)
{

}


TestFieldEncryptAnotherObject::TestFieldEncryptAnotherObject(DModel *parent)
    : DModel("TestFieldEncryptObject", parent),
    prop1(this, "prop1", true)
{

}


TestFieldEncryptAnotherObject::TestFieldEncryptAnotherObject(StoreInterface *store,
                       SyncInterface *sync,
                       const vector<string> &authorized_keys)
    : DModel("TestFieldEncryptObject", store, sync, authorized_keys),
      prop1(this, "prop1", true)
{
}


TestFieldEncryptAnother::TestFieldEncryptAnother(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestFieldEncryptAnother", "Test encryption and decryption for authorized and unauthorized objects")
{

}


void TestFieldEncryptAnother::execute() {
    // Create two test objects (to), one authorized to read encrypted data and
    // second not authorized
    TestFieldEncryptAnotherObject to_authorized1(store, sync, vector<string>());
    TestFieldEncryptAnotherObject to_authorized2(store, sync, vector<string>());
    TestFieldEncryptAnotherObject to_authorized3(store, sync, vector<string>());
    TestFieldEncryptAnotherObject to_unauthorized(store, sync, vector<string>());

    // Create one object
    vector<string> ao;
    ao.push_back(to_authorized1.get_db_id());
    ao.push_back(to_authorized2.get_db_id());
    ao.push_back(to_authorized3.get_db_id());
    TestFieldEncryptAnotherObject to(store, sync, ao);

    while (sync->process_queue());
    while (sync->process_queue());

    to.prop1 = "Ala ma kota";

    Check(to.update_authorized_objects.index(to_authorized1.get_db_id()) >= 0);
    Check(to.update_authorized_objects.index(to_authorized2.get_db_id()) >= 0);
    Check(to.update_authorized_objects.index(to_authorized3.get_db_id()) >= 0);
    Check(to.update_authorized_objects.index(to_unauthorized.get_db_id()) < 0);

    while (sync->process_queue());
    while (sync->process_queue());

    // Copy encrypted data from TestObject
    to_authorized1.set("prop1", to.get("prop1"));
    to_authorized2.set("prop1", to.get("prop1"));
    to_authorized3.set("prop1", to.get("prop1"));
    to_unauthorized.set("prop1", to.get("prop1"));

    while (sync->process_queue());
    while (sync->process_queue());

    Check(to_authorized1.get("prop1") == to.get("prop1"));
    Check(to_authorized2.get("prop1") == to.get("prop1"));
    Check(to_authorized3.get("prop1") == to.get("prop1"));
    Check(to_unauthorized.get("prop1") == to.get("prop1"));

    Check(to_authorized1.prop1.to_string() == "Ala ma kota");
    Check(to_authorized2.prop1.to_string() == "Ala ma kota");
    Check(to_authorized3.prop1.to_string() == "Ala ma kota");

    Check(string(to_unauthorized.prop1.to_string()).size() == 0);
    Check(to_unauthorized.prop1.to_string() == "");
}
