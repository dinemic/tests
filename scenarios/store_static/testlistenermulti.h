/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TESTLISTENERMULTI_H
#define TESTLISTENERMULTI_H

#include <libdinemic/dmodel.h>
#include <testbase.h>


/**
 * @brief This class adds new fields on update. Presence of those fields could be verified
 * by tests after queue is processed.
 */
class TestSpyA: public Dinemic::DAction {
    void on_create(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATE_A", "true");
    }
    void on_created(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATED_A", "true");
    }
    void on_update(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATE_A", "true");
    }
    void on_updated(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATED_A", "true");
    }
};


/**
 * @brief This class adds new fields on update. Presence of those fields could be verified
 * by tests after queue is processed.
 */
class TestSpyB: public Dinemic::DAction {
    void on_create(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATE_B", "true");
    }
    void on_created(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATED_B", "true");
    }
    void on_update(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATE_B", "true");
    }
    void on_updated(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATED_B", "true");
    }
};


/**
 * @brief This class adds new fields on update. Presence of those fields could be verified
 * by tests after queue is processed.
 */
class TestSpyC: public Dinemic::DAction {
    void on_create(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATE_C", "true");
    }
    void on_created(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATED_C", "true");
    }
    void on_update(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATE_C", "true");
    }
    void on_updated(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATED_C", "true");
    }
};


class TestListenerMulti : public TestBase
{
    TestSpyA *spy_a;
    TestSpyB *spy_b;
    TestSpyC *spy_c;
public:
    TestListenerMulti(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store);
    void prepare();
    void execute();
};

#endif // TESTLISTENERMULTI_H
