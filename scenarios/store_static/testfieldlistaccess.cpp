/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testfieldlistaccess.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestFieldListAccessObject::TestFieldListAccessObject(const string &db_id, StoreInterface *store, SyncInterface *sync)
    : DModel(db_id, store, sync),
      list1(this, "list1"),
      list2(this, "list2")
{

}

TestFieldListAccessObject::TestFieldListAccessObject(DModel *parent)
    : DModel("TestFieldAccessObject", parent),
    list1(this, "list1"),
    list2(this, "list2")
{

}

TestFieldListAccessObject::TestFieldListAccessObject(StoreInterface *store,
                       SyncInterface *sync,
                       const vector<string> &authorized_keys)
    : DModel("TestFieldAccessObject", store, sync, authorized_keys),
      list1(this, "list1"),
      list2(this, "list2")
{
}


TestFieldListAccess::TestFieldListAccess(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestFieldListAccess", "Test DList fields functionality")
{

}


void TestFieldListAccess::execute()
{
    TestFieldListAccessObject to(store, sync, vector<string>());
    to.list1.append("x");
    to.list1.append(2);

    while (sync->process_queue());
    sleep(1);

    Check(to.list_at("list1", 0) == "x");
    Check(to.list_at("list1", 1) == "2");
    Check(to.list_index("list1", "2") == 1);
    Check(to.list1.index("2") == 1);
    Check(to.list1.index("x") == 0);

    string value = to.list1.get(0);
    Check(value == "x");
    Check(to.list1.size() == 2);
    Check(to.list2.size() == 0);

    // Add lists from DModel: children, update_authorized_objects and read_authorizd_objects
    Check(to.describe_lists().size() == 5);
    Check(to.describe_lists()[3] == "list1");
    Check(to.describe_lists()[4] == "list2");
}

