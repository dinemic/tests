/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testlists.h"

using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestLists::TestLists(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestLists", "Test lists functionality")
{

}

void TestLists::execute() {
    DModel node("TestListsNode", store, sync, std::vector<std::string>());

    node.list_append("simple_list", "a");
    node.list_append("simple_list", "b");
    node.list_append("simple_list", "c");
    node.list_append("simple_list", "d");

    while (sync->process_queue());
    while (sync->process_queue());
    while (sync->process_queue());
    while (sync->process_queue());

    Check(node.list_at("simple_list", 0) == "a");
    Check(node.list_at("simple_list", 2) == "c");
    Check(node.list_at("simple_list", 3) == "d");

    node.list_delete("simple_list", 1);

    while (sync->process_queue());
    while (sync->process_queue());
    while (sync->process_queue());

    Check(node.list_at("simple_list", 0) == "a");
    Check(node.list_at("simple_list", 1) == "c");
    Check(node.list_at("simple_list", 2) == "d");

    node.list_delete("simple_list", 0);

    while (sync->process_queue());
    while (sync->process_queue());

    Check(node.list_at("simple_list", 0) == "c");
    Check(node.list_at("simple_list", 1) == "d");

    node.list_insert("simple_list", 0, "x");

    while (sync->process_queue());
    while (sync->process_queue());

    Check(node.list_at("simple_list", 0) == "x");
    Check(node.list_at("simple_list", 1) == "c");
    Check(node.list_at("simple_list", 2) == "d");
}
