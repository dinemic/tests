/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TESTNFIELDACESS_H
#define TESTNFIELDACESS_H

#include <testbase.h>
#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>

class TestFieldAccessObject : public Dinemic::DModel {
public:
    TestFieldAccessObject(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync);
    TestFieldAccessObject(Dinemic::DModel *parent=0);
    TestFieldAccessObject(Dinemic::Store::StoreInterface *store,
           Dinemic::Sync::SyncInterface *sync,
           const std::vector<std::string> &authorized_keys);

    Dinemic::DField prop1;
    Dinemic::DField prop2;
};

class TestFieldAccess : public TestBase
{
public:
    TestFieldAccess(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store);
    void prepare();
    void execute();
    void cleanup();
};

#endif // TESTNFIELDACESS_H
