/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TESTLISTENERSIMPLE_H
#define TESTLISTENERSIMPLE_H

#include <libdinemic/dmodel.h>
#include <testbase.h>
#include <check.h>

/**
 * @brief This class adds new fields on update. Presence of those fields could be verified
 * by tests after queue is processed.
 */
class TestListenerSimpleSpy: public Dinemic::DAction {
    void on_create(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATE", "true");
    }
    void on_created(Dinemic::DActionContext &context, const std::string &key) {
        context.get_object().set("CREATED", "true");
    }
    void on_update(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATE", "true");
    }
    void on_updated(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value) {
        context.get_object().set("UPDATED", "true");
    }
};


class TestListenerSimple : public TestBase
{
    TestListenerSimpleSpy *spy;
    Dinemic::DModel *not_node, *node;
public:
    TestListenerSimple(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store);
    void prepare();
    void execute();
    void cleanup();
};

#endif // TESTLISTENERSIMPLE_H
