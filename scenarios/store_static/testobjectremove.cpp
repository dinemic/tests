/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testobjectremove.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;


TestObjectRemove::TestObjectRemove(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestObjectRemove", "Test object removal")
{

}

void TestObjectRemove::execute() {
    DModel object_remove("TestObjectRemove", store, sync, vector<string>());
    object_remove.set("state", "not yet");

    while (sync->process_queue());
    Check(object_remove.get("state") == "not yet");

    object_remove.remove();

    while (sync->process_queue());

    Check(object_remove.get("state", "not defined") == "not defined");

    vector<string> keys = store->keys(&object_remove, object_remove.get_db_id() + ":*");
    Check(keys.size() == 0);
}
