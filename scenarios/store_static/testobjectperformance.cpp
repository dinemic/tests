/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testobjectperformance.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestObjectPerformance::TestObjectPerformance(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "ObjectPerformance", "Measure performance of creating new updates and processing the queue")
{

}

void TestObjectPerformance::execute() {
    DModel node("Performance", store, sync, vector<string>());
    while (sync->process_queue());

    std::string key = "performance_key_";
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < 1000; i++) {
        std::string index = to_string(i);
        node.set(key + index, index);
    }
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    cerr << "\t10k SET in " << ((double)duration/1000000) << "s" << endl;

    t1 = std::chrono::high_resolution_clock::now();
    sync->process_queue();
    t2 = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    cerr << "\t10k Process_queue in " << ((double)duration/1000000) << "s" << endl;

    t1 = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < 1000; i++) {
        std::string index = to_string(i);
        assert(node.get(key + index, "none") == index);
    }
    t2 = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    cerr << "\t10k GET in " << ((double)duration/1000000) << "s" << endl;
}
