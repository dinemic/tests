/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testfieldaccess.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestFieldAccessObject::TestFieldAccessObject(const string &db_id, StoreInterface *store, SyncInterface *sync)
    : DModel(db_id, store, sync),
      prop1(this, "prop1"),
      prop2(this, "prop2")
{

}

TestFieldAccessObject::TestFieldAccessObject(DModel *parent)
    : DModel("TestFieldAccessObject", parent),
    prop1(this, "prop1"),
    prop2(this, "prop2")
{

}

TestFieldAccessObject::TestFieldAccessObject(StoreInterface *store,
                       SyncInterface *sync,
                       const vector<string> &authorized_keys)
    : DModel("TestFieldAccessObject", store, sync, authorized_keys),
      prop1(this, "prop1"),
      prop2(this, "prop2")
{
}

TestFieldAccess::TestFieldAccess(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestFieldAccess", "Test access to the NModel through NFields")
{

}

void TestFieldAccess::prepare() {

}

void TestFieldAccess::execute() {
    TestFieldAccessObject to(store, sync, vector<string>());

    to.prop1 = "Ala ma kota";
    to.prop2 = "Kot ma ale";

    while (sync->process_queue());
    sleep(1);
    assert(to.get("prop1") == "Ala ma kota");

    string value = to.prop1.to_string();
    assert(value == "Ala ma kota");
    assert(to.prop2.to_string() == "Kot ma ale");

    assert(to.describe_fields().size() == 2);
    assert(to.describe_fields()[0] == "prop1");
    assert(to.describe_fields()[1] == "prop2");

    assert(to.serialize().find("Ala ma kota") != string::npos);
    assert(to.serialize().find("Kot ma ale") != string::npos);
}

void TestFieldAccess::cleanup() {

}
