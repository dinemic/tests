/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testlistenersmanage.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestListenersManage::TestListenersManage(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "ListenersManage", "Test add/list/remove interface for DActions and listeners")
{

}

void TestListenersManage::execute() {
    DAction a1;
    DAction a2;
    sync->add_on_create_listener("Test:*", &a1);
    sync->add_on_create_listener("Test:*", &a2);
    auto listeners = sync->get_on_create_listeners();

    string compiled_filter = Utils::wildcard_to_regex("Test:*");

    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a1)) != listeners.end());
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a1)) != listeners.end());

    // Invalid filter
    sync->remove_on_create_listener("Test:", &a1);
    listeners = sync->get_on_create_listeners();
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a1)) != listeners.end());
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a2)) != listeners.end());

    // Proper filter
    sync->remove_on_create_listener("Test:*", &a1);
    listeners = sync->get_on_create_listeners();
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a1)) == listeners.end());
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a2)) != listeners.end());

    // Try to remove non-existent listener
    sync->remove_on_create_listener("Test:*", &a1);
    listeners = sync->get_on_create_listeners();
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a1)) == listeners.end());
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a2)) != listeners.end());

    sync->remove_on_create_listener("Test:*", &a2);
    listeners = sync->get_on_create_listeners();
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a1)) == listeners.end());
    Check(find(listeners.begin(), listeners.end(), pair<string, DAction*>(compiled_filter, &a1)) == listeners.end());

    string asterisk_filter = Utils::wildcard_to_regex("*");

    auto needle = pair<string, DAction*>(asterisk_filter, &a1);

    sync->add_on_created_listener("*", &a1);
    Check(find(sync->get_on_created_listeners().begin(), sync->get_on_created_listeners().end(), pair<string, DAction*>(asterisk_filter, &a1)) != sync->get_on_created_listeners().end());
    sync->remove_on_created_listener("*", &a1);
    Check(find(sync->get_on_created_listeners().begin(), sync->get_on_created_listeners().end(), needle) == sync->get_on_created_listeners().end());

    sync->add_on_updated_listener("*", &a1);
    Check(find(sync->get_on_updated_listeners().begin(), sync->get_on_updated_listeners().end(), needle) != sync->get_on_updated_listeners().end());
    sync->remove_on_updated_listener("*", &a1);
    needle = pair<string, DAction*>(asterisk_filter, &a1);
    Check(find(sync->get_on_updated_listeners().begin(), sync->get_on_updated_listeners().end(), needle) == sync->get_on_updated_listeners().end());

    sync->add_on_update_listener("*", &a1);
    Check(find(sync->get_on_update_listeners().begin(), sync->get_on_update_listeners().end(), needle) != sync->get_on_update_listeners().end());
    sync->remove_on_update_listener("*", &a1);
    Check(find(sync->get_on_update_listeners().begin(), sync->get_on_update_listeners().end(), needle) == sync->get_on_update_listeners().end());

    sync->add_on_remove_listener("*", &a1);
    Check(find(sync->get_on_remove_listeners().begin(), sync->get_on_remove_listeners().end(), needle) != sync->get_on_remove_listeners().end());
    sync->remove_on_remove_listener("*", &a1);
    Check(find(sync->get_on_remove_listeners().begin(), sync->get_on_remove_listeners().end(), needle) == sync->get_on_remove_listeners().end());
}
