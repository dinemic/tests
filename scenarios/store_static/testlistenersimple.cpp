/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testlistenersimple.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestListenerSimple::TestListenerSimple(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "ListenerSimple", "Create simple listener for actions: create, created and updated")
{

}

void TestListenerSimple::prepare() {
    spy = new TestListenerSimpleSpy();
    sync->add_on_create_listener("TestListenerSimpleNode:*", spy);
    sync->add_on_created_listener("TestListenerSimpleNode:*", spy);
    sync->add_on_update_listener("TestListenerSimpleNode:*:value_state", spy);
    sync->add_on_updated_listener("TestListenerSimpleNotNode:*:value_state", spy);
}

void TestListenerSimple::execute() {
    // Due to change in version 02.02.2019, each change triggers local DB update
    // immediately. This causes that on create(d) listeners are called in first
    // stage of this test.

    node = new DModel("TestListenerSimpleNode", store, sync, vector<string>());
    not_node = new DModel("TestListenerSimpleNotNode", store, sync, vector<string>());
    not_node->set("state", "initializing too");

    // Here process queue
    while (sync->process_queue());
    // Here process notifications created by listener
    while (sync->process_queue());

    Check(node->get("CREATE", "none") == "true");
    Check(node->get("CREATED", "none") == "true");
    Check(node->get("UPDATE", "none") == "none");
    Check(not_node->get("UPDATED", "none") == "true");

    node->set("state", "initializing");

    while (sync->process_queue());
    while (sync->process_queue());

    Check(node->get("UPDATE", "none") == "true");
};

void TestListenerSimple::cleanup() {
    delete node;
    delete not_node;
    sync->remove_on_create_listener("TestListenerSimpleNode:*", spy);
    sync->remove_on_created_listener("TestListenerSimpleNode:*", spy);
    sync->remove_on_update_listener("TestListenerSimpleNode:[id]:value_state", spy);
    sync->remove_on_updated_listener("TestListenerSimpleNotNode:[id]:value_state", spy);
    delete spy;
}
