#include "testparentaccesschild.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestParentAccessChild::TestParentAccessChild(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestParentAccessChild", "Test accessing child's object encrypted field as parent")
{

}

void TestParentAccessChild::execute() {
    DModel parent("TestParentAccessChild", store, sync, vector<string>());

    parent.set("x", parent.encrypt("abc"));

    DModel child("Child", &parent);
    child.set("y", child.encrypt("abc"));

    Check(child.list_index("read_authorized_objects", parent.get_db_id()) >= 0);
    Check(parent.decrypt(parent.get("x")) == "abc");

    // Restore child object and try to decrypt as parent (caller)
    DModel child_restored(child.get_db_id(), store, sync, NULL, &parent);
    Check(child_restored.decrypt(child_restored.get("y")) == "abc");
}
