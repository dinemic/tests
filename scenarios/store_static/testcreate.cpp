/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testcreate.h"

using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestCreate::TestCreate(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestCreate", "Create simple object and set some values")
{

}

void TestCreate::execute() {
    DModel node("Node", store, sync, std::vector<std::string>());

    node.set("state", "initializing");
    node.set("address", "10.0.0.1");
    node.set("state", "ok");

    while (sync->process_queue());

    assert(node.get("address") == "10.0.0.1");
    assert(node.get("state") == "ok");
    assert(node.get("something", "not defined") == "not defined");

    node.del("state");
    while (sync->process_queue());

    assert(node.get("state", "not defined") == "not defined");
}
