/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TESTFIELDCALLER_H
#define TESTFIELDCALLER_H

#include <testbase.h>
#include <check.h>
#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>
#include <libdinemic/dapphelpers.h>


class TestFieldCallerObject : public Dinemic::DModel {
public:
    TestFieldCallerObject(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, Dinemic::DModel *parent, Dinemic::DModel *caller);
    TestFieldCallerObject(Dinemic::DModel *parent=0);
    TestFieldCallerObject(Dinemic::Store::StoreInterface *store,
           Dinemic::Sync::SyncInterface *sync,
           const std::vector<std::string> &authorized_keys);

    Dinemic::DField prop_encrypted;
    Dinemic::DField prop_authorized;
};

class TestFieldCaller : public TestBase
{
public:
    TestFieldCaller(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store);
    void prepare();
    void execute();
    void cleanup();
};

#endif // TESTFIELDCALLER_H
