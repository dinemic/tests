/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testobjectmemoryconsumption.h"

using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestObjectMemoryConsumption::TestObjectMemoryConsumption(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestObjectMemoryConsumption", "Check memory usage after 10k created and deleted objects")
{

}

void TestObjectMemoryConsumption::mem_usage(double &vm_usage, double &resident_set) {
    using std::ios_base;
    using std::ifstream;
    using std::string;

    unsigned long vsize;
    long rss;
    vm_usage     = 0.0;
    resident_set = 0.0;

    ifstream stat_stream("/proc/self/stat", ios_base::in);

    string pid, comm, state, ppid, pgrp, session, tty_nr;
    string tpgid, flags, minflt, cminflt, majflt, cmajflt;
    string utime, stime, cutime, cstime, priority, nice;
    string O, itrealvalue, starttime;

    stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
                >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
                >> utime >> stime >> cutime >> cstime >> priority >> nice
                >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

    stat_stream.close();

    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
    vm_usage     = vsize / 1024.0;
    resident_set = rss * page_size_kb;
}

void TestObjectMemoryConsumption::execute() {
    DModel model("TestObjectMemoryConsumption", store, sync, vector<string>());
    double vsize1, vsize2, vsize3;
    double rss1, rss2, rss3;

    cerr << "\tMemory usage for incrementing one field" << endl;

    mem_usage(vsize1, rss1);
    cerr << "\tMemory usage at beginning:\tVM: " << vsize1 << "\tRSS: " << rss1 << endl;

    for (int i = 0; i < 1000; i++) {
        model.set("a", to_string(i));
        if (i % 1000 == 0) {
            while (sync->process_queue());
            while (sync->process_queue());

            mem_usage(vsize2, rss2);
            cerr << std::fixed << std::showpoint;
            cerr << std::setprecision(2);
            cerr << "\tMemory usage after processing " << i << " messages:\tVM: " << vsize2 << "\tRSS: " << rss2 << endl;
        }
    }

    while (sync->process_queue());
    while (sync->process_queue());

    mem_usage(vsize2, rss2);
    cerr << "\tMemory usage after processing queue:\tVM: " << vsize2 << "\tRSS: " << rss2 << endl;

    model.del("a");

    while (sync->process_queue());
    while (sync->process_queue());

    mem_usage(vsize3, rss3);
    cerr << "\tMemory usage after delete:\tVM: " << vsize3 << "\tRSS: " << rss3 << endl;

    ///////////////

    cerr << "\tMemory usage for creating 10k fields" << endl;
    mem_usage(vsize1, rss1);
    cerr << "\tMemory usage at beginning:\tVM: " << vsize1 << "\tRSS: " << rss1 << endl;

    for (int i = 0; i < 1000; i++) {
        model.set("a" + to_string(i), "x");
        if (i % 1000 == 0) {
            while (sync->process_queue());
            while (sync->process_queue());

            mem_usage(vsize2, rss2);
            cerr << "\tMemory usage after creating " << i << " fields:\tVM: " << vsize2 << "\tRSS: " << rss2 << endl;
        }
    }

    while (sync->process_queue());
    while (sync->process_queue());

    mem_usage(vsize2, rss2);
    cerr << "\tMemory usage after processing queue:\tVM: " << vsize2 << "\tRSS: " << rss2 << endl;


    for (int i = 0; i < 1000; i++) {
        model.del("a" + to_string(i));
        if (i % 1000 == 0) {
            while (sync->process_queue());
            while (sync->process_queue());

            mem_usage(vsize2, rss2);
            cerr << "\tMemory usage after deleting " << i << " fields:\tVM: " << vsize2 << "\tRSS: " << rss2 << endl;
        }
    }
    model.del("a");

    while (sync->process_queue());
    while (sync->process_queue());

    mem_usage(vsize3, rss3);
    cerr << "\tMemory usage after delete:\tVM: " << vsize3 << "\tRSS: " << rss3 << endl;

}
