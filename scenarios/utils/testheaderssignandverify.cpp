/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testheaderssignandverify.h"

using namespace std;

TestHeadersSignAndVerify::TestHeadersSignAndVerify(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestHeadersSignAndVerify", "Create several signatures within one DHeaders instance and try to verify. This test also tests recreating heaers from string.")
{

}

void TestHeadersSignAndVerify::prepare() {
    string cmd = "rm -rf " + Dinemic::config.get("KEYRING_DIR");
    system(cmd.c_str());
    Dinemic::dcrypt.clear_cache();
}

void TestHeadersSignAndVerify::execute() {
    Dinemic::DHeaders headers;
    string testdheaders = Dinemic::dcrypt.create_keys("test_model");
    headers.add("Field1", "a");

    headers.add("Caller", testdheaders);

    // Headers have no Signature field - should be not verified
    Dinemic::DActionContext f0 = headers.verify(false, NULL);
    assert(f0.is_verified() == false);

    headers.sign(testdheaders);

    // Headers have Signature field and no ongoing, unsigned fields
    Dinemic::DActionContext f1 = headers.verify(false, NULL);
    assert(f1.get_verified_flags().size() == 1);
    assert(f1.is_verified() == true);

    // Again, it should not be verified - it has not confirmed field - even if
    // it has no influence to the value. This header is added after last Signature
    headers.add("Field1", "b");

    Dinemic::DActionContext f2 = headers.verify(false, NULL);
    assert(f2.get_verified_flags().size() == 2);
    assert(f2.is_verified() == false);

    // Now, it has new, second signature and it should be verified again.
    headers.sign(testdheaders);

    Dinemic::DActionContext f3 = headers.verify(false, NULL);
    assert(f3.is_verified() == true);
}
