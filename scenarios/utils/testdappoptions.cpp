/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testdappoptions.h"

using namespace std;
using namespace Dinemic;

Dinemic::TestDAppOptions::TestDAppOptions(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestDAppOptions", "Test DApp option parser")
{

}

void Dinemic::TestDAppOptions::execute() {
    vector<string> args;
    args.push_back("programName");
    args.push_back("--create");
    args.push_back("-i");
    args.push_back("-j");
    args.push_back("x");
    args.push_back("--long-switch");
    args.push_back("--long-param");
    args.push_back("--strange-value");
    args.push_back("-k");
    args.push_back("1");
    args.push_back("-k");
    args.push_back("2");

    Dinemic::DApp app(args, "TestApp", "TestApp with parameters");

    // Create, oneshot and so on should appear automatically in DApp
    app.add_option("i", "ioption", "I option switch", true);
    app.add_option("j", "joption", "J option");
    app.add_option("l", "long-switch", "Long switch", true);
    app.add_option("n", "non-present", "Non present option");
    app.add_option("p", "long-param", "Long parameter with strange value");
    app.add_option("k", "key", "Short option with multiple values");

    app.parse_options();

    Dinemic::DAppOption opt_create = app.get_option("create");

    Check(app.has_option("create"));
    Check(app.has_option("C"));
    Check(opt_create.is_present);

    Dinemic::DAppOption opt_i = app.get_option("i");
    Check(app.has_option("i"));
    Check(app.has_option("ioption"));
    Check(opt_i.is_present);

    Dinemic::DAppOption opt_j = app.get_option("j");
    Check(app.has_option("j"));
    Check(app.has_option("joption"));
    Check(opt_j.is_present);
    Check(opt_j.values.size() == 1);
    Check(opt_j.values[0] == "x");

    Dinemic::DAppOption opt_long_switch = app.get_option("long-switch");
    Check(app.has_option("long-switch"));
    Check(opt_long_switch.is_present);

    Dinemic::DAppOption opt_multiple = app.get_option("k");
    Check(app.has_option("k"));
    Check(opt_multiple.values.size() == 2);
    Check(opt_multiple.values[0] == "1");
    Check(opt_multiple.values[1] == "2");
}
