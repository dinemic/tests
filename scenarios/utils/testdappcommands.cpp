#include "testdappcommands.h"

using namespace Dinemic;
using namespace std;

string model;
string command;
string param;

TestDAppCommandModel::TestDAppCommandModel(string db_id, Store::StoreInterface *store, Sync::SyncInterface *sync)
    : DModel(db_id, store, sync, NULL, NULL)
{

}

TestDAppCommandModel::TestDAppCommandModel(Store::StoreInterface *store, Sync::SyncInterface *sync, std::vector<string> authorized_objects)
    : DModel("TestDAppCommandModel", store, sync, authorized_objects)
{

}

void TestDAppCommandModel::handle_command(DApp *app, std::vector<string> commands) {
    if (commands.size() != 3) {
        return;
    }
    model = commands[0];
    command = commands[1];
    param = commands[2];
}

TestDAppCommands::TestDAppCommands(Sync::SyncInterface *sync, Store::StoreInterface *store)
    : TestBase(sync, store, "TestDAppCommands", "Test non-option parameters for DApp class")
{
}

void TestDAppCommands::execute() {
    vector<string> args;
    args.push_back("programName");
    args.push_back("--option");
    args.push_back("testModel");
    args.push_back("command");
    args.push_back("one");

    Dinemic::DApp app(args, "TestApp", "TestApp with parameters");

    //TestDAppCommandModel dmodel(app.get_store_interface(), app.get_sync_interface(), std::vector<std::string>());

    vector<string> mapping, options;
    mapping.push_back("testModel");
    mapping.push_back("command");
    options.push_back("--option");
    app.add_command(mapping, &TestDAppCommandModel::handle_command);

    app.exec();
    Check(app.get_commands().size() == 3);

    Check(model == "testModel");
    Check(command == "command");
    Check(param == "one");
}
