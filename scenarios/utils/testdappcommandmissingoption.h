/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TESTDAPPCOMMANDMISSINGOPTION_H
#define TESTDAPPCOMMANDMISSINGOPTION_H

#include <check.h>
#include <testbase.h>
#include <libdinemic/dapp.h>
#include <libdinemic/dmodel.h>


namespace Dinemic {
    class TestDAppCommandOptionModel : public Dinemic::DModel {
    public:
        TestDAppCommandOptionModel(Store::StoreInterface *store, Sync::SyncInterface *sync, std::vector<std::string> authorized_objects);
        TestDAppCommandOptionModel(std::string db_id, Store::StoreInterface *store, Sync::SyncInterface *sync);

        static void handle_command(DApp *app, std::vector<std::string> commands);
    };

    class TestDAppCommandMissingOption : public TestBase
    {
        void option_missing_not_defined();
        void option_present_not_defined();
        void option_present_and_defined();
    public:
        TestDAppCommandMissingOption(Sync::SyncInterface *sync, Store::StoreInterface *store);
        void execute();
    };
}

#endif // TESTDAPPCOMMANDMISSINGOPTION_H
