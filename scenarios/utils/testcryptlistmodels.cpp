/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testcryptlistmodels.h"

using namespace std;

TestCryptListModels::TestCryptListModels(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestCryptKeyCreate", "Test creating key pairs")
{

}

void TestCryptListModels::prepare() {
    string cmd = "rm -rf \"" + Dinemic::config.get("KEYRING_DIR") + "\"";
    system(cmd.c_str());
    Dinemic::dcrypt.clear_cache();
}

void TestCryptListModels::execute() {
    string test1 = Dinemic::dcrypt.create_keys("test_model");
    string test2 = Dinemic::dcrypt.create_keys("test_model");
    string test3 = Dinemic::dcrypt.create_keys("test_model");

    vector<string> keys = Dinemic::dcrypt.list_owned_keys();
    // Should be found:
    for (auto k : keys) {
        ERROR(k);
    }

    ERROR("Got: "+ test1);
    assert(find(keys.begin(), keys.end(), test1) != keys.end());
    assert(find(keys.begin(), keys.end(), test2) != keys.end());
    assert(find(keys.begin(), keys.end(), test3) != keys.end());

    // Should NOT be found:
    assert(find(keys.begin(), keys.end(), "test4") == keys.end());
    assert(find(keys.begin(), keys.end(), "") == keys.end());
    assert(find(keys.begin(), keys.end(), ".") == keys.end());
    assert(find(keys.begin(), keys.end(), "..") == keys.end());
}
