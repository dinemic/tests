/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testcryptsignature.h"

using namespace std;

TestCryptSignature::TestCryptSignature(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestCryptSignature", "Test creating signatures")
{

}


void TestCryptSignature::prepare() {
    string cmd = "rm -rf " + Dinemic::config.get("KEYRING_DIR");
    system(cmd.c_str());
}


void TestCryptSignature::execute() {
    string test_key = Dinemic::dcrypt.create_keys("test_model");
    string another_key = Dinemic::dcrypt.create_keys("test_model");

    string signature = Dinemic::dcrypt.sign("ala ma kota", test_key);
    assert(Dinemic::dcrypt.verify("ala ma kota", signature, test_key) == true);
    assert(Dinemic::dcrypt.verify("ala ma kota", signature, another_key) == false);
    try {
        Dinemic::dcrypt.verify("ala ma kota", signature, "not_known");
        cerr << "Non-existing key found!" << endl;
        throw exception();
    } catch (Dinemic::DEncryptKeyNotFound e) {

    } catch (Dinemic::DException &e) {

    }
}

