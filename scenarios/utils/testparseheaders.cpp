/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testparseheaders.h"

using namespace std;

TestParseHeaders::TestParseHeaders(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestUtilsParseHeaders", "Test headers parser for malformed messages")
{

}

void TestParseHeaders::execute() {
    string message;
    Dinemic::DHeaders h1("Field1: Value1");
    Check(h1["Field1"] == "Value1");

    Dinemic::DHeaders h2("Field1: Value1\n");
    Check(h2["Field1"] == "Value1");

    Dinemic::DHeaders h3("Field1:Value1");
    Check(h3.fields().size() == 0);
    Check(h3["Field1"] == "");

    Dinemic::DHeaders h4("Field1: Value1\nField2: Value2");
    Check(h4["Field1"] == "Value1");
    Check(h4["Field2"] == "Value2");

    Dinemic::DHeaders h5("Field1: Value1\nField1: Value2");
    Check(h5["Field1"] == "Value2");
    Check(h5.get_all("Field1")[0] == "Value1");
    Check(h5.get_all("Field1")[1] == "Value2");

    Dinemic::DHeaders h6("Field1: Value1\n\nField2: Value2");
    Check(h6["Field1"] == "Value1");
    Check(h6["Field2"] == "Value2");

    Dinemic::DHeaders h7("Field1: Value1\n\n\n\nField2: Value2");
    Check(h7["Field1"] == "Value1");
    Check(h7["Field2"] == "Value2");

    Dinemic::DHeaders h8("Field1: Value1\nxyz\nField2: Value2");
    Check(h8["Field1"] == "Value1");
    Check(h8["Field2"] == "Value2");

    Dinemic::DHeaders h9(": \nxyz\nField2: Value2");
    Check(h9["Field2"] == "Value2");

    Dinemic::DHeaders h10("Empty: ");
    Check(h10["Empty"] == "");
    Check(h10.fields().size() == 1);

    Dinemic::DHeaders h11("Multi: split: Field");
    Check(h11["Multi"] == "split: Field");

    Dinemic::DHeaders h12("Multi: split: Field1: Field2");
    Check(h12["Multi"] == "split: Field1: Field2");

    Dinemic::DHeaders h13(": ");
    Check(h13.fields().size() == 0);

    Dinemic::DHeaders h14(": : : ");
    Check(h14.fields().size() == 0);

    Dinemic::DHeaders h15(": \n: \n: ");
    Check(h15.fields().size() == 0);

    Dinemic::DHeaders h16("asdasd");
    Check(h16.fields().size() == 0);

    Dinemic::DHeaders h17("asd\n");
    Check(h17.fields().size() == 0);

    Dinemic::DHeaders h18("asd\nasd");
    Check(h18.fields().size() == 0);

    Dinemic::DHeaders h19("#Comment: value\n#Second comment: abc\nField: value");
    Check(h19.fields().size() == 1);
    Check(h19["Field"] == "value");

    Dinemic::DHeaders h20;
    h20.add("Field", "Value");
    Check(h20.to_string().find("Value") != string::npos);
    Dinemic::config.set("PRESHARED_KEY", "PjtmEYPzs4M0R8vzJylKGmw9OssdIc5K/fx6w7LwTMY=");
    Check(h20.to_string().find("Field") == string::npos);
    Check(Dinemic::DHeaders(h20.to_string())["Field"] == "Value");
}
