/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testcryptstore.h"

using namespace std;

TestCryptStore::TestCryptStore(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestCryptStore", "Test DCrypt class functionality")
{

}

void TestCryptStore::prepare() {
    string cmd = "rm -rf " + Dinemic::config.get("KEYRING_DIR");
    Dinemic::Utils::exec(cmd);
    Dinemic::dcrypt.clear_cache();
}

void TestCryptStore::execute() {
    // First should fail on wrong key name (not model:id)
    try {
        Dinemic::dcrypt.store_key("test_key", "public_key_value", "public_sign_value", "private_key_value", "private_sign_value");
        throw exception();
    } catch (Dinemic::DException &e) {
        // ok
    }
    Dinemic::dcrypt.store_key("test_key:id", "public_key_value", "public_sign_value", "private_key_value", "private_sign_value");
    Dinemic::dcrypt.store_key("test_public:id", "public_key_value", "public_sign_value");
    Dinemic::dcrypt.store_key("some_key:id", "a", "b", "c", "d");

    try {
        Dinemic::dcrypt.store_key("test/key", "public_enc", "public_sig", "private_enc", "private_sig");
        assert("Stored with invlid name" == NULL);
    } catch (exception e) {

    }

    try {
        Dinemic::dcrypt.store_key("test_key", "public_enc", "public_sig", "private_enc", "private_sig");
        assert("Stored duplicate" == NULL);
    } catch (exception e) {

    }

    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR")));
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id"));
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@encryption_public"));
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@encryption_secret"));
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@sign_public"));
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@sign_secret"));

    Check(!boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/test/key"));

    char buffer[1024];
    std::ifstream public_file(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@encryption_public", std::ifstream::binary);
    public_file.read(buffer, 1000);
    Check(string(buffer, strlen("public_key_value")) == "public_key_value");

    std::ifstream secret_file(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@encryption_secret", std::ifstream::binary);
    secret_file.read(buffer, 1000);
    Check(string(buffer, strlen("private_key_value")) == "private_key_value");

    std::ifstream sign_public_file(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@sign_public", std::ifstream::binary);
    sign_public_file.read(buffer, 1000);
    Check(string(buffer, strlen("public_sign_value")) == "public_sign_value");

    std::ifstream sign_secret_file(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@sign_secret", std::ifstream::binary);
    sign_secret_file.read(buffer, 1000);
    Check(string(buffer, strlen("private_sign_value")) == "private_sign_value");

    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testpublic/id/@sign_public") == true);
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testpublic/id/@encryption_public") == true);
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testpublic/id/@sign_secret") == false);
    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testpublic/id/@encryption_secret") == false);

    Check(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/somekey/id/@encryption_secret"));
}
