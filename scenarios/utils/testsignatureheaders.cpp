/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testsignatureheaders.h"

using namespace std;

TestSignatureHeaders::TestSignatureHeaders(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestSignatureHeaders", "Test signing update stored with DHeaders structure")
{

}

void TestSignatureHeaders::prepare() {
    Dinemic::Utils::exec("rm -rf " + Dinemic::config.get("KEYRING_DIR"));
}


void TestSignatureHeaders::execute() {
    Dinemic::DCrypt dc1, dc2;
    string TestSignatureHeaders = dc1.create_keys("test_model");

    Dinemic::DHeaders headers;
    headers.add("A", "a");
    headers.add("B", "b");
    headers.add(HEADER_CALLER, TestSignatureHeaders);
    headers.add(HEADER_SIGNATURE_METHOD, "DCrypt");
    headers.add(HEADER_SIGNATURE, Dinemic::Utils::escape_value(dc1.sign(headers.to_string_unsigned(), TestSignatureHeaders)));

    // Test with cached keys
    bool verified1 = dc1.verify(headers.to_string_unsigned(), Dinemic::Utils::unescape_value(headers[HEADER_SIGNATURE]), headers[HEADER_CALLER]);

    // Test with non-cached keys
    bool verified2 = dc2.verify(headers.to_string_unsigned(), Dinemic::Utils::unescape_value(headers[HEADER_SIGNATURE]), headers[HEADER_CALLER]);
    assert(verified1 == true);
    assert(verified2 == true);
}
