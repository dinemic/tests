#include "testdappcommandmissingoption.h"

using namespace Dinemic;
using namespace std;


TestDAppCommandOptionModel::TestDAppCommandOptionModel(string db_id, Store::StoreInterface *store, Sync::SyncInterface *sync)
    : DModel(db_id, store, sync, NULL, NULL)
{

}


TestDAppCommandOptionModel::TestDAppCommandOptionModel(Store::StoreInterface *store, Sync::SyncInterface *sync, std::vector<string> authorized_objects)
    : DModel("TestDAppCommandOptionModel", store, sync, authorized_objects)
{

}


void TestDAppCommandOptionModel::handle_command(DApp *app, std::vector<string> commands) {
}


TestDAppCommandMissingOption::TestDAppCommandMissingOption(Sync::SyncInterface *sync, Store::StoreInterface *store)
    : TestBase(sync, store, "TestDAppCommandMissingOption", "Test option validation for commands")
{
}

void TestDAppCommandMissingOption::option_missing_not_defined() {
    vector<string> args;
    args.push_back("programName");
    args.push_back("testModel");
    args.push_back("command");
    args.push_back("one");

    Dinemic::DApp app(args, "TestApp", "TestApp with parameters");

    vector<string> mapping, options;
    mapping.push_back("testModel");
    mapping.push_back("command");
    options.push_back("option");
    app.add_command(mapping, &TestDAppCommandOptionModel::handle_command, options);

    int rc = app.exec();
    Check(rc == 1);
}

void TestDAppCommandMissingOption::option_present_not_defined() {
    vector<string> args;
    args.push_back("programName");
    args.push_back("testModel");
    args.push_back("command");
    args.push_back("--option");
    args.push_back("one");

    Dinemic::DApp app(args, "TestApp", "TestApp with parameters");

    vector<string> mapping, options;
    mapping.push_back("testModel");
    mapping.push_back("command");
    options.push_back("option");
    app.add_command(mapping, &TestDAppCommandOptionModel::handle_command, options);

    int rc = app.exec();
    Check(rc == 1);
}

void TestDAppCommandMissingOption::option_present_and_defined() {
    vector<string> args;
    args.push_back("programName");
    args.push_back("testModel");
    args.push_back("command");
    args.push_back("--option");
    args.push_back("one");

    Dinemic::DApp app(args, "TestApp", "TestApp with parameters");

    //TestDAppCommandModel dmodel(app.get_store_interface(), app.get_sync_interface(), std::vector<std::string>());

    vector<string> mapping, options;
    mapping.push_back("testModel");
    mapping.push_back("command");
    options.push_back("option");
    app.add_option("", "option", "Test option", true, Dinemic::DAppOptionMode_any);
    app.add_command(mapping, &TestDAppCommandOptionModel::handle_command, options);

    int rc = app.exec();
    Check(rc == 0);
}

void TestDAppCommandMissingOption::execute() {
    option_missing_not_defined();
    option_present_not_defined();
    option_present_and_defined();
}
