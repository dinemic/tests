/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testcryptremovekey.h"

using namespace std;

TestCryptRemoveKey::TestCryptRemoveKey(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestCryptRemoveKey", "Test key removal")
{

}

void TestCryptRemoveKey::prepare() {
    string cmd = "rm -rf " + Dinemic::config.get("KEYRING_DIR");
    Dinemic::Utils::exec(cmd);
    Dinemic::dcrypt.clear_cache();
}

void TestCryptRemoveKey::execute() {
    Dinemic::dcrypt.store_key("test_key:id", "public_key_value", "public_sign_value", "private_key_value", "private_sign_value");
    assert(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR")));
    assert(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/"));
    assert(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@encryption_public"));
    assert(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@encryption_secret"));
    assert(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@sign_public"));
    assert(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/@sign_secret"));


    Dinemic::dcrypt.remove_key("test_key:id");
    assert(boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR")));
    assert(!boost::filesystem::exists(Dinemic::config.get("KEYRING_DIR") + "/testkey/id/"));
}
