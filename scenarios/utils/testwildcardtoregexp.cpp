/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "testwildcardtoregexp.h"

using namespace std;

TestWildcardToRegexp::TestWildcardToRegexp(Dinemic::Sync::SyncInterface *sync, Dinemic::Store::StoreInterface *store)
    : TestBase(sync, store, "TestWildcardToRegexp", "Test converting wildcards and dinemic tokens to regular expressions")
{

}

void TestWildcardToRegexp::execute()
{
    Check(Dinemic::Utils::wildcard_to_regex("*") == "^.+$");
    Check(Dinemic::Utils::wildcard_to_regex("?") == "^.$");
    Check(Dinemic::Utils::wildcard_to_regex("Test:*") == "^Test\\:.+$");
    Check(Dinemic::Utils::wildcard_to_regex("Test:[id]") == "^Test\\:[a-zA-Z0-9_]+$");
    Check(Dinemic::Utils::wildcard_to_regex("") == ".*");

/*    boost::regex pattern1(Utils::wildcard_to_regex("Test:[id]"));
    Check(boost::regex_match("Test:123abcABC123", pattern1));

    boost::regex pattern2(Utils::wildcard_to_regex("Test:[ID]"));
    Check(boost::regex_match("Test:123abcABC123", pattern2));

    boost::regex pattern3(Utils::wildcard_to_regex("[object]"));
    Check(boost::regex_match("Test:123abcABC123", pattern3));

    boost::regex pattern4(Utils::wildcard_to_regex("[obj]"));
    Check(boost::regex_match("Test:123abcABC123", pattern4));

    boost::regex pattern5(Utils::wildcard_to_regex("[OBJECT]"));
    Check(boost::regex_match("Test:123abcABC123", pattern5));

    boost::regex pattern6(Utils::wildcard_to_regex("[OBJ]"));
    Check(boost::regex_match("Test:123abcABC123", pattern6));

    boost::regex pattern7(Utils::wildcard_to_regex("[obj]:[field]property"));
    Check(boost::regex_match("ModelName:6571284ioerq3ufjaj98:value_property", pattern7));*/
}
