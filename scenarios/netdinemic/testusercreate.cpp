/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testusercreate.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;


TestNetDinemicUserCreate::TestNetDinemicUserCreate(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestUserCreate", "Test creating users via netdinemic")
{

}

void TestNetDinemicUserCreate::prepare() {
    env1.add_var("DINEMIC_CONFIG", "config.1.dinemic");
    env2.add_var("DINEMIC_CONFIG", "config.2.dinemic");

    env1.call_sync("rm -rf /tmp/netdinemic.1/*");
    env2.call_sync("rm -rf /tmp/netdinemic.2/*");
}

void TestNetDinemicUserCreate::execute() {
    env1.call_async("./netdinemic --launch");
    env2.call_async("./netdinemic --launch");
    sleep(5);
    string new_user = env1.call_sync("./netdinemic --create --user --user-first-name First1 --user-last-name Last1 --user-email test@cloudover.io");
    string id = new_user.substr(0, new_user.size()-1);

    sleep(5);
    string result1 = env1.call_sync("./netdinemic --oneshot --user-list");
    string result2 = env2.call_sync("./netdinemic --oneshot --user-list");

    assert(result1.find("true") != string::npos);
    assert(result2.find("false") != string::npos);
    assert(result2.find(id.substr(0, id.size()-1)) != string::npos);
}

void TestNetDinemicUserCreate::cleanup() {
    env1.kill_procs();
    env2.kill_procs();
    char proc_path[1024];
    proc_path[readlink("/proc/self/exe", proc_path, 1024)] = 0x00;
    env1.call_sync(string(proc_path) + " cleanup");
    env2.call_sync(string(proc_path) + " cleanup");
}
