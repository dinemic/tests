/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testnodecreate.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestNetDinemicNodeCreate::TestNetDinemicNodeCreate(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestNodeCreate", "Test creating and listing nodes on multiple hosts")
{

}

void TestNetDinemicNodeCreate::prepare() {
    env1.add_var("DINEMIC_CONFIG", "config.1.dinemic");
    env2.add_var("DINEMIC_CONFIG", "config.2.dinemic");

    env1.call_sync("rm -rf /tmp/netdinemic.1/*");
    env2.call_sync("rm -rf /tmp/netdinemic.2/*");
}

void TestNetDinemicNodeCreate::execute() {
    env1.call_async("./netdinemic --launch");
    env2.call_async("./netdinemic --launch");

    sleep(5);

    string new_user = env1.call_sync("./netdinemic --create --user --user-first-name First1 --user-last-name Last1 --user-email test@cloudover.io");
    string user_id = new_user.substr(0, new_user.size()-1);

    string new_node = env1.call_sync(string("") + "./netdinemic --create --node --user-id " + user_id);
    assert(new_node.size() > 0);

    vector<string> node_lines = Utils::split(new_node, "\n");
    assert(node_lines.size() > 1);

    string node_id = node_lines[node_lines.size()-2].substr(0, node_lines[node_lines.size()-2].size());

    sleep(3);
    string nodes1 = env2.call_sync("./netdinemic --oneshot --node-list");
    string nodes2 = env2.call_sync("./netdinemic --oneshot --node-list");
    assert(nodes1.find("true") != string::npos);
    assert(nodes2.find("false") != string::npos);
    assert(nodes2.find(node_id) != string::npos);
}

void TestNetDinemicNodeCreate::cleanup() {
    env1.kill_procs();
    env2.kill_procs();
    char proc_path[1024];
    proc_path[readlink("/proc/self/exe", proc_path, 1024)] = 0x00;
    env1.call_sync(string(proc_path) + " cleanup");
    env2.call_sync(string(proc_path) + " cleanup");
}
