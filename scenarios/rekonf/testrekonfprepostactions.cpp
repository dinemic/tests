/**
Copyright 2017-2019 cloudover.io ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "testrekonfprepostactions.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestRekonfPrePostActions::TestRekonfPrePostActions(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestRekonPrePostActions", "Test executing pre and post actions on file update")
{

}

void TestRekonfPrePostActions::prepare() {
    env1.call_sync("rm -rf /tmp/test_rekonf_tests.1");
    env2.call_sync("rm -rf /tmp/test_rekonf_tests.2");

    env1.add_var("DINEMIC_CONFIG", "config.1.dinemic");
    env2.add_var("DINEMIC_CONFIG", "config.2.dinemic");
}

void TestRekonfPrePostActions::execute() {
    string json;

    env1.call_async("rekonf --launch 2> /dev/null");
    env2.call_async("rekonf --launch 2> /dev/null");

    sleep(5);
    string admin_id = env1.call_sync("rekonf --create --admin-create --admin-name TestAdmin --admin-email admin@dinemic.io");
    admin_id.pop_back();

    // WARNING: If copying this as example to your application - remember about
    // possible shell injection here! IDs in database could be forged by attacker
    string node_id = env1.call_sync(string("rekonf --create --node-create --admin-id ") + admin_id);
    node_id.pop_back();

    string file_id = env1.call_sync(string("rekonf --create --file-create /tmp/test1 --admin-id ") + admin_id);
    file_id.pop_back();

    sleep(2);

    env1.call_sync(string("rekonf --oneshot --node-id ") + node_id + " --file-id " + file_id + " --admin-id " + admin_id + " --node-add-file");

    env1.call_sync(string("rekonf --oneshot --admin-id ") + admin_id + " --file-id " + file_id + " --file-pre-update 'echo abc > /tmp/pre'");
    env1.call_sync(string("rekonf --oneshot --admin-id ") + admin_id + " --file-id " + file_id + " --file-post-update 'echo abc > /tmp/post'");
    env1.call_sync(string("rekonf --oneshot --admin-id ") + admin_id + " --file-id " + file_id + " --file-edit " + "abc");

    sleep(5);
    Check(boost::filesystem::exists("/tmp/pre"));
    Check(boost::filesystem::exists("/tmp/post"));
    Check(boost::filesystem::exists("/tmp/test1"));
}

void TestRekonfPrePostActions::cleanup() {
    env1.kill_procs();
    env2.kill_procs();
    Utils::exec("killall rekonf");
}
