#include "testrekonfseparateadminandfile.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

TestRekonfSeparateAdminAndFile::TestRekonfSeparateAdminAndFile(SyncInterface *sync, StoreInterface *store)
    : TestBase(sync, store, "TestSeparateAdminAndFile", "Test creating admin and file in separate environments and then assigning them to node")
{

}

void TestRekonfSeparateAdminAndFile::prepare() {
    envAdmin.call_sync("rm -rf /tmp/test_rekonf_tests.1");
    envFile.call_sync("rm -rf /tmp/test_rekonf_tests.2");
    envNode.call_sync("rm -rf /tmp/test_rekonf_tests.3");
    envNode.call_sync("rm -rf /tmp/dinemic/keys/*");
    envNode.call_sync("rm -rf /tmp/rekonf_test_pre");
    envNode.call_sync("rm -rf /tmp/rekonf_test_post");

    envAdmin.add_var("DINEMIC_CONFIG", "config.1.dinemic");
    envFile.add_var("DINEMIC_CONFIG", "config.2.dinemic");
    envNode.add_var("DINEMIC_CONFIG", "config.3.dinemic");

    envAdmin.call_sync("redis-cli flushall");
    envFile.call_sync("redis-cli flushall");
    envNode.call_sync("redis-cli flushall");
    
    Utils::exec("killall rekonf");
    Utils::exec("/usr/bin/redis-cli flushall");
}

void TestRekonfSeparateAdminAndFile::execute() {
    string json;

    envAdmin.call_async("rekonf --launch 2> /dev/null");
    envFile.call_async("rekonf --launch 2> /dev/null");
    envNode.call_async("rekonf --launch 2> /dev/null");

    sleep(5);
    string admin_id = envAdmin.call_sync("rekonf --create --admin-create --admin-name TestAdmin --admin-email admin@dinemic.io");
    admin_id.pop_back();

    sleep(5);
    // Check for admin presence on other instances

    string node_id = envNode.call_sync(string() + "rekonf --create --node-create --admin-id " + admin_id);
    node_id.pop_back();
    string file_id = envFile.call_sync(string() + "rekonf --create --file-create /tmp/rekonf_test_file --file-pre-update 'touch /tmp/rekonf_test_pre' --file-post-update 'touch /tmp/rekonf_test_post' --admin-id " + admin_id);
    file_id.pop_back();

    sleep(5);
    envAdmin.call_sync(string() + "rekonf --oneshot --admin-id " + admin_id + " --file-id " + file_id + " --node-id " + node_id + " --node-add-file");
    sleep(5);
    envAdmin.call_sync(string() + "rekonf --oneshot --admin-id " + admin_id + " --file-id " + file_id + " --file-edit abc");
    sleep(5);

    string file_contents = envNode.call_sync("cat /tmp/rekonf_test_file");
    Check(file_contents.find("abc") != string::npos);
}

void TestRekonfSeparateAdminAndFile::cleanup() {
    envAdmin.kill_procs();
    envFile.kill_procs();
    envNode.kill_procs();
    Utils::exec("killall rekonf");
}
